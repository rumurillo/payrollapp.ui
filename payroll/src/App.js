import React, { Component } from 'react';
import './App.css';
import TextField, {HelperText, Input} from '@material/react-text-field';
import Button from '@material/react-button';
import axios from 'axios';
import MaterialTable from 'material-table'

class App extends Component {

  constructor(props){
    super(props);
    this.state = { value: ''};
    this.state = { users : [] };
  }

  findEmployees = () => {
    if(this.state.value === undefined || this.state.value === '')
    {
        axios.get('https://payrollapp-api.azurewebsites.net/api/employees/getemployees')
        .then(users =>{
          this.setState({ users : users.data });
        });
    }
    else
      axios.get('https://payrollapp-api.azurewebsites.net/api/employees/getemployee?id=' + this.state.value)
      .then(users => {
        this.setState({users:[users.data]});
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Payroll App
          </p>
          <TextField
                onTrailingIconSelect={() => this.setState({value: ''})}>
                <Input type="number" value={this.state.value} onChange={(e) => this.setState({value: e.target.value})} />
          </TextField>
          <div>
              <Button onClick={this.findEmployees}>Find Employee(s)</Button>
          </div>
          <MaterialTable
              columns={[
                { title: 'Id', field: 'Id' },
                { title: 'Name', field: 'Name' },
                { title: 'ContractTypeName', field: 'ContractTypeName' },
                { title: 'RoleId', field: 'RoleId' },
                { title: 'RoleName', field: 'RoleName' },
                { title: 'RoleDescription', field: 'RoleDescription' },
                { title: 'HourlySalary', field: 'HourlySalary' },
                { title: 'MonthlySalary', field: 'MonthlySalary' },
                { title: 'Salary', field: 'Salary' }]}
              data={this.state.users}
              title="Payroll Data"
            />
        </header>
      </div>
    );
  }
}

export default App;